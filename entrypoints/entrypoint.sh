#!/bin/bash

set -e

export ADDONS_PATH=$(python3.8 /opt/odoo/pol-goroda/scripts/parse_project.py)

# check connection with database
python3.8 /opt/odoo/pol-goroda/scripts/wait_for_psql.py \
    --db_host=${DB_HOST} \
    --db_port=${DB_PORT} \
    --db_user=${DB_USER} \
    --db_password=${DB_PASSWORD} \
    --timeout 30

python3.8 /opt/odoo/pol-goroda/vendor/odoo/cc/odoo-bin \
    -c /opt/odoo/pol-goroda/odoo.conf \
    --db_host=${DB_HOST} \
    --db_password=${DB_PASSWORD} \
    --db_user=${DB_USER} \
    --db_port=${DB_PORT} \
    --longpolling-port=${LONGPOLLING_PORT} \
    --addons-path=${ADDONS_PATH} \
    --dev=all

exit 1
