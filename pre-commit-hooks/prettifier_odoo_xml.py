""" Designed to convert xml documents to a beautiful view """


# Stdlib:
import argparse
import re
import sys
from xml.dom import minidom
from xml.sax.saxutils import escape, unescape

# Thirdparty:
import black

DEFAULT_LINE_LENGTH = 80
MAIN_INDENT = " " * 4
ATTR_DELIMITER = " "
OPEN_OPENING_TAG = "<"
OPEN_CLOSING_TAG = "</"
CLOSE_TAG = ">"
CLOSE_TAG_HIMSELF = "/>"
FIELD_NAMES_PY_EXPRESSION = {"context", "domain"}
ATTRS_NAMES_PY_EXPRESSION = {
    "context",
    "domain",
    "attrs",
    "eval",
    "invisible",
    "create",
    "edit",
    "delete",
    "required",
    "readonly",
    "password",
    "filter_domain",
    "attachment",
    "t-if",
    "t-elif",
    "t-else",
    "t-esc",
    "t-foreach",
    "t-value",
    "t-options",
    "decoration-bf",
    "decoration-it",
    "decoration-danger",
    "decoration-info",
    "decoration-muted",
    "decoration-primary",
    "decoration-success",
    "decoration-warning",
}


class ErrorList:
    """ Singlton: list that stores errors

    Raises:
        StopIteration: Raise when we went through all the elements
        TypeError: Raise when they try to work with this class not like an array
    """

    def __new__(cls):
        if not hasattr(cls, "instance"):
            cls.instance = super(ErrorList, cls).__new__(cls)
            cls._errors_list = []
            cls._cursor = -1
        return cls.instance

    def append(self, element):
        """ Add new element to array

        Arguments:
            element {str} -- Error that need add to common list
        """

        self._errors_list.append(element)

    def clear(self):
        """ Сlears the error list """

        self._errors_list.clear()

    def __bool__(self):
        return bool(self._errors_list)

    def __repr__(self):
        return str(self._errors_list)

    def __iter__(self):
        self._cursor = -1
        return self

    def __next__(self):
        self._cursor += 1
        if self._cursor >= len(self._errors_list):
            raise StopIteration()
        return self._errors_list[self._cursor]

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._errors_list[item]
        raise TypeError(
            "'{}' object is not subscriptable, but you can use indexes".format(
                type(self).__name__
            )
        )


class ModeBlack:
    """ Mode for Black """

    def __new__(cls):
        if not hasattr(cls, "instance"):
            cls.instance = super(ModeBlack, cls).__new__(cls)
            mode = black.FileMode()
            mode.string_normalization = False
            mode.line_length = DEFAULT_LINE_LENGTH
            cls._mode = mode
        return cls.instance

    def get_mode(self):
        """ Get mode for Black """

        return self._mode


def _overflowed(len_line, max_chars=DEFAULT_LINE_LENGTH):
    """ Сhecks if the string length is less than the specified limit

    Arguments:
        len_line {int} -- Length of string to compare

    Keyword Arguments:
        max_chars {int} -- maximum number of characters that can be in a string (default: {DEFAULT_LINE_LENGTH})

    Returns:
        boolean -- True if overfloved else False
    """

    return len_line > max_chars


def _prettify_css(self, attr_name, css, open_tag, indent="", addindent="", newl=""):
    """ Brings css code in a beautiful view

    Arguments:
        attr_name {str} -- Attribute name, need for calculating max line length
        css {str} -- CSS code
        open_tag {str} -- Current node name


    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})

    Returns:
        str -- Beautiful CSS code
    """

    def prettify_css_inline(css):
        """ Formats css code in one line

        Arguments:
            css {str} -- CSS code

        Returns:
            str -- Beautiful CSS code
        """

        css = re.sub(r"\s+", " ", css)
        css = re.sub(r" {2,}", " ", css)
        css = re.sub(r" *\( *", "(", css)
        css = re.sub(r" *\) *", ")", css)
        css = re.sub(r" *: *", ": ", css)
        css = re.sub(r" *\, *", ", ", css)
        css = re.sub(r" *; *", "; ", css).strip()
        if css[-1] != ";":
            css += ";"
        return css

    def convert_single_line_to_multi_line(css):
        """ Converts css code from single-line to multi-line: each key on a new line

        Arguments:
            css {str} -- CSS code

        Returns:
            str -- Beautiful CSS code
        """

        new_indent = "".join([indent, addindent, addindent])
        css = "".join([newl, new_indent, css, " "])
        css = re.sub(r"; ", ";{}{}".format(newl, new_indent), css)[: -len(addindent)]
        return css

    css = prettify_css_inline(css)
    attribute_name_length = len(attr_name)
    tag_length = len(open_tag) + (
        len(CLOSE_TAG) if self.childNodes else len(CLOSE_TAG_HIMSELF)
    )
    if not _overflowed(len(css) + tag_length + attribute_name_length):
        return css
    return convert_single_line_to_multi_line(css)


def _css_attribute_handler(
    self, key, value, open_tag, indent="", addindent="", newl=""
):
    """ Parses an attribute with CSS code in the value

    Arguments:
        key {str} -- Attribute name
        value {str} -- Attribute value
        open_tag {str} -- Current node name

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})

    Returns:
        str -- Attribute with formatted CSS code in the value
    """

    return '{}="{}"'.format(
        key,
        escape(
            _prettify_css(self, key, value, open_tag, indent, addindent, newl),
            {'"': "&quot;"},
        ),
    )


def _python_attribute_handler(self, key, value, indent="", addindent="", newl=""):
    """ Parses an attribute with python code in the value

    Arguments:
        key {str} -- Attribute name
        value {str} -- Attribute value

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})

    Returns:
        str -- Attribute with formatted via Black python code in the value
    """

    try:
        formatted_value_black = escape(
            black.format_str(
                unescape(value, {"&quot;": '"'}), mode=ModeBlack().get_mode()
            ),
            {'"': "&quot;"},
        )
    except black.InvalidInput:
        formatted_value_black = "..."
        ErrorList().append(
            (
                '"Black" cannot parse this attribute value: "{}", '
                "its should be python code, please fix this"
                '\nNode: "{}", Attribute: "{}", Value: "{}"'
            ).format(unescape(value), self.tagName, key, repr(value))
        )
    if formatted_value_black.count("\n") > 1:
        parts = [i for i in formatted_value_black.split("\n") if i]
        formatted_value = newl
        new_indent = "".join([indent, addindent, addindent])
        for part in parts:
            formatted_value = "".join([formatted_value, new_indent, part, newl])
        attribute = '{}="{}{}"'.format(key, formatted_value, indent + addindent)
    else:
        attribute = '{}="{}"'.format(key, formatted_value_black[: -len("\n")])
    return attribute


def _other_cases_attribute_handler(key, value):
    """ Parses an attribute

    Arguments:
        key {str} -- Attribute name
        value {str} -- Attribute value

    Returns:
        str -- Attribute with formatted value
    """

    value = re.sub(r"\s{2,}", " ", value)
    attribute = '{}="{}"'.format(key, escape(value.strip(), {'"': "&quot;"}))
    return attribute


def _python_text_handler(self, data, indent="", addindent="", newl=""):
    """ Parses a value as python code

    Arguments:
        data {str} -- Python code

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})

    Returns:
        str -- beautiful python code
    """

    try:
        formatted_value_black = escape(
            black.format_str(
                unescape(data, {"&quot;": '"'}), mode=ModeBlack().get_mode()
            ),
            {'"': "&quot;"},
        )
    except black.InvalidInput:
        formatted_value_black = ""
        ErrorList().append(
            (
                '"Black" cannot parse this node value: "{}", '
                "its should be python code, please fix this"
                '\nNode: "{}", Value: "{}"'
            ).format(unescape(data), self.parentNode.tagName, repr(data))
        )
    if formatted_value_black.count("\n") > 1:
        parts = [i for i in formatted_value_black.split("\n") if i]
        formatted_value = newl
        for part in parts:
            formatted_value = "".join(
                [formatted_value, "".join([indent, addindent]), part, "\n"]
            )
        return "".join([formatted_value, indent])
    return formatted_value_black[:-1]


def _whitespaces_text_handler(self, data, newl=""):
    """ Parses a value as whitespaces

    Arguments:
        data {str} -- Whitespaces

    Keyword Arguments:
        newl {str} -- Newline string (default: {""})

    Returns:
        str -- Whitespaces
    """

    if data.count(newl) > 1 and len(self.parentNode.childNodes) > 1:
        return newl
    return ""


def _other_cases_text_handler(self, data, indent, newl=""):
    """ Parses a value: removes extra whitespaces

    Arguments:
        data {str} -- Value of parent node

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        newl {str} -- Newline string (default: {""})

    Returns:
        str -- Value without extra whitespaces
    """

    value = "".join(re.split(r"[ \t\v\f\r]+$", data))
    tag_length = getattr(self.parentNode, "full_tag_length", DEFAULT_LINE_LENGTH + 1)
    if newl in [value[: len(newl)], value[-len(newl) :]]:
        formatted_value = re.sub(r"^(" + re.escape(newl) + r")*", "", value.strip())
        formatted_value = re.sub(r"(" + re.escape(newl) + r")*$", "", formatted_value)
        if not formatted_value.count(newl):
            if not _overflowed(len(formatted_value) + tag_length):
                value = formatted_value
            else:
                value = "".join([newl, value.strip(), newl, indent])
    else:
        if not value.count(newl):
            if _overflowed(len(value) + tag_length):
                value = "".join([newl, value.strip(), newl, indent])
    if value[-len(newl) :] == newl:
        value = "".join([value, indent])
    return escape(value, {'"': "&quot;"})


# pylint: disable=invalid-name
def _attributes_handler(self, open_tag, indent="", addindent="", newl=""):
    """ Parses attributes and makes them look beautiful

    Arguments:
        open_tag {str} -- Current node name

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})

    Returns:
        list[str] -- List of all attributes of the current node
    """

    attrs = self._get_attributes()
    full_tag_length = len(open_tag) + (
        len(CLOSE_TAG) if self.childNodes else len(CLOSE_TAG_HIMSELF)
    )
    attrs_for_writing = []
    for key, value in attrs.items():
        if key in ATTRS_NAMES_PY_EXPRESSION:
            attribute = _python_attribute_handler(
                self, key, value, indent, addindent, newl
            )
        elif key == "style":
            attribute = _css_attribute_handler(
                self, key, value, open_tag, indent, addindent, newl
            )
        else:
            attribute = _other_cases_attribute_handler(key, value)
        full_tag_length += len(attribute) + len(ATTR_DELIMITER)
        attrs_for_writing.append(attribute)
    self.full_tag_length = full_tag_length
    return attrs_for_writing


def _write_attributes(self, writer, open_tag, indent="", addindent="", newl=""):
    """ Writes attributes to the current node

    Arguments:
        writer {StringIO} -- Standart variable from origin writexml method of minidom.
        writes text to the buffer
        open_tag {str} -- Current node name

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})
    """

    attrs_for_writing = _attributes_handler(self, open_tag, indent, addindent, newl)
    if _overflowed(getattr(self, "full_tag_length", DEFAULT_LINE_LENGTH + 1)):
        writer.write(newl)
        for attr in attrs_for_writing:
            writer.write("".join([indent, MAIN_INDENT, attr, newl]))
        writer.write(indent)
    elif attrs_for_writing:
        writer.write(ATTR_DELIMITER + ATTR_DELIMITER.join(attrs_for_writing))


def _write_opening_tag(self, writer, nodes, indent="", addindent="", newl=""):
    """ Writes an opening tag of the current node

    Arguments:
        writer {StringIO} -- Standart variable from origin writexml method of minidom.
        writes text to the buffer
        nodes {NodeList} -- List of all children of the current node

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})
    """

    open_tag = "".join([OPEN_OPENING_TAG, self.tagName])
    writer.write("".join([indent, open_tag]))
    _write_attributes(self, writer, open_tag, indent, addindent, newl)

    if nodes and not (
        len(nodes) == 1
        and isinstance(nodes[0], minidom.Text)
        and nodes[0].data.isspace()
    ):
        writer.write(CLOSE_TAG)


def _write_tag_value(self, writer, nodes, indent="", addindent="", newl=""):
    """ Writes the contents of the current node.
        Will call writexml for each child, and also check for errors

    Arguments:
        writer {StringIO} -- Standart variable from origin writexml method of minidom.
        writes text to the buffer
        nodes {NodeList} -- List of all children of the current node

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})
    """

    if nodes:
        if len(nodes) == 1 and nodes[0].nodeType == minidom.Node.TEXT_NODE:
            nodes[0].writexml(writer, indent, addindent, newl)
        elif nodes:
            writer.write(newl)
            for node in nodes:
                if isinstance(node, minidom.Text):
                    # if current node contain value and nodes then you cannot commit
                    if not node.data.isspace():
                        ErrorList().append(
                            'error! please add any tag for value: "{}" of node: "{}"'.format(
                                node.data, self.tagName
                            )
                        )
                    node.writexml(writer, "", addindent, newl)
                else:
                    node.writexml(writer, indent + addindent, addindent, newl)
            writer.write(indent)


def _write_closing_tag(self, writer, nodes, newl=""):
    """ Сloses the current opening tag

    Arguments:
        writer {StringIO} -- Standart variable from origin writexml method of minidom.
        writes text to the buffer
        nodes {NodeList} -- List of all children of the current node
    Keyword Arguments:
        newl {str} -- Newline string (default: {""})
    """

    if (
        len(nodes) == 1
        and isinstance(nodes[0], minidom.Text)
        and nodes[0].data.isspace()
    ) or not nodes:
        writer.write("".join([CLOSE_TAG_HIMSELF, newl]))
    else:
        writer.write("".join([OPEN_CLOSING_TAG, self.tagName, CLOSE_TAG, newl]))


def writexml_element(self, writer, indent="", addindent="", newl=""):
    """ Writes a new node to the buffer of StringIO

    Arguments:
        writer {StringIO} -- Standart variable from origin writexml method of minidom.
        writes text to the buffer

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})
    """

    _write_opening_tag(self, writer, self.childNodes, indent, addindent, newl)
    _write_tag_value(self, writer, self.childNodes, indent, addindent, newl)
    _write_closing_tag(self, writer, self.childNodes, newl)


def writexml_text(self, writer, indent="", addindent="", newl=""):
    """Writes a value of node to the buffer of StringIO

    Arguments:
        writer {StringIO} -- Standart variable from origin writexml method of minidom.
        writes text to the buffer

    Keyword Arguments:
        indent {str} -- Current indentation (default: {""})
        addindent {str} -- Indentation to add to higher levels (default: {""})
        newl {str} -- Newline string (default: {""})
    """

    origin_value = self.data
    name_attribute_name = None
    if self.parentNode and self.parentNode.tagName:
        name_attribute_name = {
            k: v for k, v in self.parentNode._get_attributes().items()
        }.get("name")
    if name_attribute_name and (
        (
            self.parentNode.tagName == "attribute"
            and name_attribute_name in ATTRS_NAMES_PY_EXPRESSION
        )
        or (
            self.parentNode.tagName == "field"
            and name_attribute_name in FIELD_NAMES_PY_EXPRESSION
        )
    ):
        value = _python_text_handler(self, origin_value, indent, addindent, newl)
    elif origin_value.isspace():
        value = _whitespaces_text_handler(self, origin_value, newl)
    else:
        value = _other_cases_text_handler(self, origin_value, indent, newl)

    writer.write(value)


def prettify(document):  # noqa: C901
    """ Return a pretty-printed XML string from the document

    Arguments:
        document {Document} -- Xml document that will be formatted

    Returns:
        str -- Pretty document
    """

    ErrorList().clear()
    minidom.Element.writexml = writexml_element
    minidom.Text.writexml = writexml_text
    dom = minidom.parseString(document)
    pretty_xml = dom.toprettyxml(
        indent=MAIN_INDENT, newl="\n", encoding="utf-8"
    ).decode("utf-8")
    return pretty_xml


def main(argv=None):
    """ Makes more beautiful the code of the transferred files """

    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="*", help="XML filenames to check.")
    args = parser.parse_args(argv)
    ret_val = 0
    console_delimiter_len = 120
    for file_name in args.filenames:
        pretty_xml = ""
        with open(file_name, encoding="utf-8") as xml_file:
            not_pretty_xml = xml_file.read()
            pretty_xml = prettify(not_pretty_xml)
        if not ErrorList():
            with open(file_name, "wt", encoding="utf-8") as xml_file:
                xml_file.write(pretty_xml)
        else:
            ret_val = 1
            # pylint: disable=print-used
            print(
                "*" * console_delimiter_len, "\n", 'file name: "{}"'.format(file_name)
            )
            for error in ErrorList():
                # pylint: disable=print-used
                print("_" * console_delimiter_len, "\n", error)
    if ret_val:
        # pylint: disable=print-used
        print("_" * console_delimiter_len)
    return ret_val


if __name__ == "__main__":
    sys.exit(main())
