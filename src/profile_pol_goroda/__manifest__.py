{
    "name": "Profile pol-goroda",
    "version": "1.0",
    "depends": ["base", "sale_management", "website", "website_sale_comparison"],
    "data": [
        "data/00_product_data.xml",
        "data/01_product_attributes_data.xml",
        "data/02_groups_data.xml",
        "data/03_localization_data.xml",
    ],
    "application": True,
    "installable": True,
}
